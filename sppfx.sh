#!/bin/bash

# VARIABLES TO BE SET UP BY USER ##########################################
# If needed, adjust the defaults here in the script or override them by
# setting the environment variables STEAM_LIBRARY_PATH, PROTON_PATH and/or
# STEAM_PATH before executing sppfx

# Steam library path (directory containing a steamapps directory):
STEAM_LIBRARY_PATH_DEFAULT="$HOME/.steam/steam"

# Path to the winetricks command -- by default we'll just run "winetricks", 
# expecting it to be in the user's $PATH:
WINETRICKS_COMMAND_DEFAULT="winetricks"

# Proton installation path (usually found in the same Steam library as
# the first game used with the desired Proton version, but custom Proton
# builds are possible):
PROTON_PATH_DEFAULT="$HOME/.steam/steam/steamapps/common/Proton 4.2"

# Steam installation path:
STEAM_PATH_DEFAULT="$HOME/.steam"

# Steam app IDs: Add your game IDs, one per line, as below (no spaces)
war="230410" # Warframe
skyrimse="489830" # Skyrim Special Edition
nier="524220" # NieR: Automata
ds1="211420" # Dark Souls: Prepare to Die Edition
ds2="335300" # Dark Souls II: Scholar of the First Sin
ds3="374320" # Dark Souls III
fo4="377160" # Fallout 4

# END OF USER SETUP #######################################################

# Some initial path checking:

if [ -z "$STEAM_PATH" ] ; then
  STEAM_PATH="$STEAM_PATH_DEFAULT"
fi
if [ -d "$STEAM_PATH" ] ; then
  echo "[sppfx] Using Steam installation at: $STEAM_PATH" 
else
  echo "[sppfx] ERROR: STEAM_PATH not found: $STEAM_PATH" >&22
  exit 1
fi

if [ -z "$STEAM_LIBRARY_PATH" ] ; then
  STEAM_LIBRARY_PATH="$STEAM_LIBRARY_PATH_DEFAULT"
fi
if [ -d "$STEAM_LIBRARY_PATH" ] ; then
  echo "[sppfx] Using Steam library at: $STEAM_LIBRARY_PATH" 
else
  echo "[sppfx] ERROR: STEAM_LIBRARY_PATH not found: $STEAM_LIBRARY_PATH" >&2 
  exit 1
fi

if [ -z "$PROTON_PATH" ] ; then
  PROTON_PATH="$PROTON_PATH_DEFAULT"
fi
if [ -d "$PROTON_PATH/dist" ] ; then
  echo "[sppfx] Using Proton installation at: $PROTON_PATH" 
else
  echo "[sppfx] ERROR: No dist directory found at PROTON_PATH: $PROTON_PATH" >&2 
  exit 1
fi

PROTON_PATH="$PROTON_PATH/dist" # append /dist so user doesn't have to

export PATH="$PROTON_PATH/bin:$PATH"

echo "[sppfx] Using PATH: $PATH"
echo "[sppfx] Using wine executable: $(command -v wine)"

# Runtime path stuff:

export WINEDLLPATH="$PROTON_PATH/lib64/wine:$PROTON_PATH/lib/wine"

export LD_LIBRARY_PATH="$PROTON_PATH/lib64:$PROTON_PATH/lib:$STEAM_PATH/ubuntu12_32:$STEAM_PATH/ubuntu12_32/panorama:$STEAM_PATH/ubuntu12_32/steam-runtime/pinned_libs_32:$STEAM_PATH/ubuntu12_32/steam-runtime/pinned_libs_64:/usr/lib/x86_64-linux-gnu/libfakeroot:/lib/i386-linux-gnu:/usr/lib/i386-linux-gnu:/usr/local/lib:/lib/x86_64-linux-gnu:/usr/lib/x86_64-linux-gnu:/lib32:/usr/lib32:/lib:/usr/lib:/usr/lib/i386-linux-gnu/tls:/usr/lib/x86_64-linux-gnu/tls:$STEAM_PATH/ubuntu12_32/steam-runtime/i386/lib/i386-linux-gnu:$STEAM_PATH/ubuntu12_32/steam-runtime/i386/lib:$STEAM_PATH/ubuntu12_32/steam-runtime/i386/usr/lib/i386-linux-gnu:$STEAM_PATH/ubuntu12_32/steam-runtime/i386/usr/lib:$STEAM_PATH/ubuntu12_32/steam-runtime/amd64/lib/x86_64-linux-gnu:$STEAM_PATH/ubuntu12_32/steam-runtime/amd64/lib:$STEAM_PATH/ubuntu12_32/steam-runtime/amd64/usr/lib/x86_64-linux-gnu:$STEAM_PATH/ubuntu12_32/steam-runtime/amd64/usr/lib:$LD_LIBRARY_PATH"

# Add DXVK to DLL overrides unless user doesn't want us to:
if [[ "$SPPFX_USE_WINED3D" != "1" ]] ; 
then
  export WINEDLLOVERRIDES="d3d11,dxgi,d3d10,d3d10_1,d3d10core=n;$WINEDLLOVERRIDES" 
fi
echo "[sppfx] Using DLL overrides: $WINEDLLOVERRIDES"

FIRSTARG=true # Bool for the loop
CMDSTRING="" # String for the commands to run
NEEDTOADDWINE=true # Do we need to prepend "wine" to the command?

while [[ "$#" > 0 ]] # Loop through args
do
  if [[ $FIRSTARG == true ]] # If first argument
  then
    id=""
    if [ -z ${!1+x} ] # If first arg is not an id variable name
    then
      if [[ $1 =~ ^[0-9]+$ ]] # if arg is a number
      then
        id=$1 # set $id to the argument string
      else
        echo "[sppfx] ERROR: There is no ID variable for: $1, and it is not a valid ID." >&2 
        exit 1
      fi
    else
      id=${!1} # set $id to the variable value
    fi
    # export STEAM_COMPAT_DATA_PATH="$STEAM_LIBRARY_PATH/steamapps/compatdata"
	# (we're not using the "proton" script anyway, so this var shouldn't matter)
    export WINEPREFIX="$STEAM_LIBRARY_PATH/steamapps/compatdata/$id/pfx" # Set the wine prefix environment variable
    if [ -d "$WINEPREFIX" ] ; then
      echo "[sppfx] Using wineprefix at: $WINEPREFIX"
    else
      # trying create a prefix 
      mkdir -p "$WINEPREFIX"
      wine64 $PROTON_PATH/share/default_pfx/drive_c/windows/system32/winboot.exe -u
      if [ -d "$WINEPREFIX"/drive_c ] ; then
        echo "[sppfx] Using wineprefix at: $WINEPREFIX"
      else
        echo "[sppfx] ERROR: Can't create WINEPREFIX at: $WINEPREFIX" >&2 
        echo "[sppfx] ERROR: WINEPREFIX not found: $WINEPREFIX" >&2 
        rm -rf $WINEPREFIX
        exit 1
      fi
    fi
    FIRSTARG=false
  else # If not first argument
    if [[ $1 == "wine" || $1 == "winetricks" ]]
    then
      NEEDTOADDWINE=false # Don't prepend "wine" to command
    fi
    if [[ $1 == "winetricks" ]] ; then
      if [ -z "$WINETRICKS_COMMAND" ] ; then
        WINETRICKS_COMMAND="$WINETRICKS_COMMAND_DEFAULT"
      fi
      command -v $WINETRICKS_COMMAND >/dev/null 2>&1 || { echo >&2 "[sppfx] ERROR: WINETRICKS_COMMAND unusable: $WINETRICKS_COMMAND"; exit 1; }
      echo "[sppfx] Using winetricks command: $WINETRICKS_COMMAND"
      CMDSTRING="$CMDSTRING \"$WINETRICKS_COMMAND\"" # Add the winetricks command to the command string
    else	
      CMDSTRING="$CMDSTRING \"$1\"" # Add the argument to the commands to be run
	fi
  fi
  shift; # Shift along the arguments
done

CMDSTRING="${CMDSTRING#"${CMDSTRING%%[![:space:]]*}"}"

if [[ $NEEDTOADDWINE == true ]]
then
  CMDSTRING="wine $CMDSTRING" # Prepend "wine" to command
fi
echo "[sppfx] Running command: $CMDSTRING"
bash -c "$CMDSTRING" # Run the commands
